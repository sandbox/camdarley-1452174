(function($) {

  var DerivativesStatusInterval = 0;
  
  Drupal.behaviors.media_derivatives_status_progress = {
  
    attach: function(context, settings) {
      
      var derivatives_list = Drupal.settings.media_derivatives_status.derivatives_list;
      
      var fid = Drupal.settings.media_derivatives_status.fid;
      
      
      var ajax_url = Drupal.settings.basePath+"admin/media_derivatives_status/ajax/ajax/"+fid+"/"+derivatives_list[0]+"/actualize";
      
      if (!DerivativesStatusInterval) {
 	    // Set the interval on which to call this function again.
 	    DerivativesStatusInterval = setInterval(function() {
 	      $.getJSON(ajax_url, function(data) {
 	        $("#" + derivatives_list[0] + " .percentage").html(data.percent + "%");
 	        $("#" + derivatives_list[0] + " .filled").attr("style", "width: " + data.percent + "%");
 	        if (data.percent >= 100) {
 	          $(derivatives_list[0] + "_media-derivatives-status-wrapper").remove();
 	          clearInterval(DerivativesStatusInterval);
 	          //$(derivatives_list[0]+" fieldset").append("<a href="">remove</a>");
 	        }
          });
 	    }, 3000);
 	  }
       
    }
    
  }
  

}(jQuery)); 